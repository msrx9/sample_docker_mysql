## コマンド

### mysqlサーバの起動　
```
docker-compose up -d
```

### 停止
```
docker-compose stop
```

---
## mysql-clientからのアクセス

```
# rootログイン
#   password: root
mysql -h 0.0.0.0 -u root -p

# testログイン
#   password: test
mysql -h 0.0.0.0 -u test -p

```

---

### DBeaverでみる場合の設定
![hoge](./img/dbeaver_1.png)

`MySQL 8.x` を選びます。

![hoge](./img/dbeaver_2.png)

|設定項目|設定値|
|---|---|
|Server Host|0.0.0.0|
|Port|3306|
|Database|sample_db|
|User name|test|
|Password|test|

上記の通りに入力して、`完了` です。
