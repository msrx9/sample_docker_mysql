
-- データベース sample_db を作成、選択
create database sample_db;
use sample_db;

-- データベース sample_db に テーブル fruits を作成
CREATE TABLE fruits (
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  name varchar(20)
);

-- 権限設定
GRANT ALL PRIVILEGES ON sample_db.* TO test@'%';
FLUSH PRIVILEGES;
